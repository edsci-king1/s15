	// 1. Create variables to store to the following user details:

	let firstName = 'First Name: Mariano Luiz';
	console.log(firstName)
	let lastName = 'Last Name: King';
	console.log(lastName)
	let age = 'Age: 17'; 
	console.log(age)
	// a number, not a string
	console.log("Hobbies:")
	let hobbies = ['Movies', 'Sleeping', 'Chilling', 'Anime'];
	console.log(hobbies) 
	// each hobby should be a string, enclosed in quotes

	console.log("Work Address:");
	let workAddress = {
	    // object properties should be enclosed in quotes
	    houseNumber: 'Block 31 Lots 1 and 2',
	    street: 'Narra',
	    city: 'Caloocan',
	    state: 'Metro Manila',
	};
	console.log(workAddress);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "My full name is: Steve Rogers";
	console.log(fullName);

	let currentAge = "My current age is: 40";
	console.log(currentAge);
	
	console.log("My Friends are:");
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log(friends);

	let fullProfile = "My Full Profile:";
	console.log(fullProfile);

	let profile = {
		userName: 'captain_america',
		fullName: 'Steve Rogers',
		age: 40,
		isActive: false,
	};
	console.log(profile);

	let bestFriend = "My bestfriend is: Bucky Barnes";
	console.log(bestFriend);

	let lastLocation = "Arctic Ocean";
	const lastFound = "I was found frozen in: " + lastLocation;	
	console.log(lastFound);

	